﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers
{
    public static class LimitHelpers 
    {
        public static void CancelPartnerLimit(Partner partner)
        {
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                activeLimit.CancelDate = DateTime.Now;
            }
        }

        public static PartnerPromoCodeLimit SetPartnerLimit(Partner partner, SetPartnerPromoCodeLimitRequest request)
        {
            CancelPartnerLimit(partner);

            partner.NumberIssuedPromoCodes = 0;

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = request.EndDate
            };

            partner.PartnerLimits.Add(newLimit);

            return newLimit;
        }
    }
}