﻿using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Common;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitTests
    {
        private PartnersController _partnerController;
        private Mock<IRepository<Partner>> _partnerRepositoryMock = new();

        public SetPartnerPromoCodeLimitTests()
        {
            _partnerController = new PartnersController(_partnerRepositoryMock.Object);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_PartnerIsNotFound_ReturnsResultWithStatusCode404NotFound()
        {
            var partner = null as Partner;
            var limit = GetValidSetPartnerPromoCodeLimitRequest();

            _partnerRepositoryMock
                .Setup(m => m.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(default, limit);

            GetStatusCode(result).Should().Be(StatusCodes.Status404NotFound);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_PartnerIsNotActive_ReturnsResultWithStatusCode400BadRequest()
        {
            Partner partner = PartnerFactory.GetValidPartner();
            partner.IsActive = false;
            var limit = GetValidSetPartnerPromoCodeLimitRequest();

            _partnerRepositoryMock
                .Setup(m => m.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(default, limit);

            GetStatusCode(result).Should().Be(StatusCodes.Status400BadRequest);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_PartnerHasActiveLimit_ResetsToZeroNumberIssuedPromoCodes()
        {
            var partner = PartnerFactory.GetValidPartner();
            var limit = GetValidSetPartnerPromoCodeLimitRequest();

            _partnerRepositoryMock
                .Setup(m => m.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(default, limit);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_PartnerHasActiveLimit_SetsCancelDateForActiveLimit()
        {
            var partner = PartnerFactory.GetValidPartner();
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
            var previousLimit = new PartnerPromoCodeLimit();
            partner.PartnerLimits.Add(previousLimit);
            var limit = GetValidSetPartnerPromoCodeLimitRequest();

            _partnerRepositoryMock
                .Setup(m => m.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(default, limit);

            previousLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_NewLimitLessThanOrEqualToZero_ReturnsResultWithStatusCode400BadRequest()
        {
            var partner = PartnerFactory.GetValidPartner();

            _partnerRepositoryMock
                .Setup(m => m.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(default, new() { Limit = 0 });

            GetStatusCode(result).Should().Be(StatusCodes.Status400BadRequest);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_SetNewLimit_EnsureLimitSavedInDatabase()
        {
            var partner = PartnerFactory.GetValidPartner();

            var serviceProvider = GetServiceProvider();
            var repository = serviceProvider.GetService<IRepository<Partner>>();

            await repository.AddAsync(partner);

            var limit = GetValidSetPartnerPromoCodeLimitRequest();

            var partnerController = new PartnersController(repository);
            var createdResult = await partnerController
                .SetPartnerPromoCodeLimitAsync(partner.Id, limit);

            var limitId = (Guid)(createdResult as CreatedAtActionResult).RouteValues["limitId"];

            var result = (await partnerController.GetPartnerLimitAsync(partner.Id, limitId)).Result;

            GetStatusCode(result).Should().Be(StatusCodes.Status200OK);
        }

        private int? GetStatusCode(IActionResult result)
            => result switch
            {
                StatusCodeResult s => s.StatusCode,
                ObjectResult o => o.StatusCode,
                _ => null
            };

        private SetPartnerPromoCodeLimitRequest GetValidSetPartnerPromoCodeLimitRequest()
            => new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = new DateTime(2036, 12, 31),
                Limit = 9
            };

        private IServiceProvider GetServiceProvider()
            => new ServiceCollection()
                .ConfigureInMemoryContext()
                .AddEfRepository()
                .BuildServiceProvider();
    }
}
