﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public static class TestsConfiguration
    {
        public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection services)
        {
            var servicesProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            services.AddDbContext<DataContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDb", builder => { });
                options.UseInternalServiceProvider(servicesProvider);
            });

            services.AddTransient<DbContext, DataContext>();

            return services;
        }

        public static IServiceCollection AddEfRepository(this IServiceCollection services)
        {
            return services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
        }

    }
}
