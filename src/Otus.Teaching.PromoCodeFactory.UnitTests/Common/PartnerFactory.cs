﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Common
{
    internal class PartnerFactory
    {
        internal static Partner GetValidPartner() => new Partner()
        {
            Id = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165"),
            Name = "FrightfulFirework",
            IsActive = true,
            NumberIssuedPromoCodes = 1,
            PartnerLimits = new List<PartnerPromoCodeLimit>()
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2022, 02, 24),
                    EndDate = new DateTime(2024, 03, 17),
                    Limit = 200
                }
            }
        };
    }
}
