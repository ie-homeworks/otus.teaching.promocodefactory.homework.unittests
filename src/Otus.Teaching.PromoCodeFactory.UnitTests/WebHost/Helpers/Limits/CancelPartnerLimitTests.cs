﻿using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Common;
using Otus.Teaching.PromoCodeFactory.WebHost.Helpers;
using System;
using System.Collections.Generic;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Helpers.Limits
{
    public class CancelPartnerLimitTests
    {
        [Fact]
        public void CancelPartnerLimit_PartnerHasActiveLimit_SetsCancelDateForActiveLimit()
        {
            var partner = PartnerFactory.GetValidPartner();
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
            var previousLimit = new PartnerPromoCodeLimit();
            partner.PartnerLimits.Add(previousLimit);

            LimitHelpers.CancelPartnerLimit(partner);

            previousLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public void CancelPartnerLimit_PartnerIsNull_ThrowsNullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() => LimitHelpers.CancelPartnerLimit(null));
        }
    }
}
