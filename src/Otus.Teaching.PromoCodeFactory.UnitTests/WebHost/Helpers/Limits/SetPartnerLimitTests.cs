﻿using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Common;
using Otus.Teaching.PromoCodeFactory.WebHost.Helpers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Helpers.Limits
{
    public class SetPartnerLimitTests
    {
        [Fact]
        public void SetPartnerLimit_ValidArguments_ReturnsValidPromoCodeLimit()
        {
            var partner = PartnerFactory.GetValidPartner();
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = new DateTime(2036, 12, 31),
                Limit = 9
            };

            var expected = new PartnerPromoCodeLimit()
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = request.EndDate
            };

            var actual = LimitHelpers.SetPartnerLimit(partner, request);

            expected.Limit.Should().Be(actual.Limit);
            expected.EndDate.Should().Be(actual.EndDate);
        }
    }
}
